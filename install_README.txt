To manually install the asset pack, read the Asset_Pack_Installation.pdf file.

To automatically install the asset pack:
- Windows: Double-click the install_windows.bat file
- Linux: Run the install_linux.py file ($ python3 install_linux.py)

You can view the source code for the asset installers in our GitHub repository: https://github.com/PineappleTF/AssetInstaller